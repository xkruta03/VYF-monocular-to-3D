
# Depth Map Prediction from a Single Image using a Multi-Scale Deep Network

This project is an adjusted implementation of the paper called "Depth Map Prediction from a Single Image using a Multi-Scale Deep Network",  for details see Acknowledgments.

### Prerequisites

For the training, you will need the NYU dataset downloaded and processed in the project root directory.

### Training the CNN

Training can be done by:

```
python VYF_mono_to3D.py
```
Be aware that the training script will load any existing model that is already in model folder and pick up where it ended. The app will infrom you how well is it doing in comparison to the testing images. Every 100 iterations a cross entropy is evaluated against the training images. Every 1000 iteration the error against testing images is evaluated and model is saved in model folder.

### Evaluating model

For the evaluation purposes the ImageTest.py script was created. The script is run as follows:

```
python ImageTest.py -i <inputFile.png> -o <resultFile.png>
```

The script expects the model to be in model folder at the same level as the script itself.

## License

This project is licensed under the MIT License

## Acknowledgments

* The project is an adjusted implementation of the original article ["Depth Map Prediction from a Single Image using a Multi-Scale Deep Network"](http://papers.nips.cc/paper/5539-depth-map-prediction-from-a-single-image-using-a-multi-scale-deep-network.pdf)
* Using the [NYU dataset](https://cs.nyu.edu/~silberman/datasets/nyu_depth_v2.html)
* Splitting the dataset is done with help of the script from folks from University of Bonn
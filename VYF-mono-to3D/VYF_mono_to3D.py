from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import re
import sys
import tarfile
import random
from PIL import Image


import data_loader as dl
import model as m
import constanst as c

import numpy as np

import tensorflow as tf

FLAGS = None

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

BATCH_S = 32 #number of batches for evalutation
BATCH_T = 16 #number of batches for learning

train_data_directory = os.path.join(ROOT_DIR, "outNYU/training")
test_data_directory = os.path.join(ROOT_DIR, "outNYU/testing")

def SaveImageG(img, name,minn=0,maxx=0):

    if (minn==0 and maxx==0):
        maxx = np.max(img)
        minn = np.min(img)

    img = (img-minn) * (255./(maxx-minn))
    img=np.squeeze(img,2)
    im = Image.fromarray(img).convert('RGB')
    im.save(name)
    return minn,maxx

def SaveImage(img, name):
    maxx = np.max(img)
    minn = np.min(img)

    img = (img-minn) * (255./(maxx-minn))
    img = np.uint8(img)
    im = Image.fromarray(img)
    im.save(name)    


def main(unused_args):
    print("Reading learn data")
    images, depthsImages = dl.load_data(train_data_directory)

    print("Reading test data")
    imagesTest, depthsImagesTest = dl.load_data(test_data_directory)
    #print(len(imagesTest),len(depthsImagesTest))   
   
    gpu_options = tf.GPUOptions(allow_growth=True)

    inputs = tf.placeholder(tf.float32, shape=[None, c.IMAGE_WIDTH, c.IMAGE_HEIGHT,3], name = 'inputs')
    depths = tf.placeholder(tf.float32, shape=[None, c.IMAGE_DWIDTH, c.IMAGE_DHEIGHT,1], name = 'depths')
    learn_ratio = tf.placeholder(tf.float32, name='learn_ratio')
    learn_ratio_arch = tf.placeholder(tf.float32, name='learn_ratio_arch')
    keep_prob = tf.placeholder(tf.float32, name='keep_prob')
    is_training = tf.placeholder(tf.bool, name='is_training')
    

    with tf.variable_scope('coarse_arch'):    
        net = m.coarse_arch(inputs,keep_prob)

    with tf.variable_scope('fine_arch'):    
        logits = m.fine_arch(inputs,net)

    with tf.variable_scope('loss_coarse_arch'):    
        cross_entropy_mean_coarse_arch = m.lossFunction(net,depths)

    with tf.variable_scope('loss_fine_arch'):    
        cross_entropy_mean_fine_arch = m.lossFunction(logits,depths)


    variables_coarse_arch = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,scope='coarse_arch')
    variables_fine_arch = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,scope='fine_arch')

    with tf.variable_scope('train_variables_coarse_arch'):    
        train_step = tf.train.AdamOptimizer(learn_ratio).minimize(cross_entropy_mean_coarse_arch,var_list=variables_coarse_arch) 
    with tf.variable_scope('train_variables_fine_arch'):    
        train_step_arch = tf.train.AdamOptimizer(learn_ratio_arch).minimize(cross_entropy_mean_fine_arch,var_list=variables_fine_arch) 



    with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:
        sess.run(tf.global_variables_initializer())

        saver = tf.train.Saver()
        print("Restore net")
        saver.restore(sess, save_path="model/model")
        for i in range(100000):            
            inp=[]
            outp=[]
            for j in range(BATCH_S):
                rnd = random.randint(0,len(images)-1) 
                img2, img2Depth = dl.DataAugmentation(images[rnd],depthsImages[rnd])
                inp.append(img2)
                outp.append(img2Depth)
            res,res1,res2 = sess.run([train_step,train_step_arch,cross_entropy_mean_fine_arch],feed_dict={
                                inputs: inp,
                                depths: outp,
                                keep_prob: 0.5, 
                                learn_ratio: 0.00001,
                                learn_ratio_arch: 0.00001,
                                is_training: True
                              });
            if(i%100==0):
                print(i, 'Itererations ',res2)
                eval = sess.run(logits,feed_dict={
                                inputs: inp,
                                keep_prob: 1, 
                                is_training: False
                              });
                minn,maxx = SaveImageG(outp[0],'res/o{}.png'.format(i))
                SaveImageG(eval[0],'res/r{}.png'.format(i),minn,maxx)
                SaveImage(inp[0],'res/i{}.png'.format(i))

            if (i%1000==0):  
                inpTest=[]
                outpTest=[]
                for j in range(BATCH_T):
                    rnd = random.randint(0,len(imagesTest)-1) 
                    img2, img2Depth = dl.DataAugmentation(imagesTest[rnd],depthsImagesTest[rnd])
                    inpTest.append(img2)
                    outpTest.append(img2Depth)

                eval,res = sess.run([logits,cross_entropy_mean_fine_arch],feed_dict={
                                inputs: inpTest,
                                depths: outpTest,
                                keep_prob: 1, 
                                is_training: False
                              });
                print("Evaluation: ",i,"Error: ",res)
                for ii in range(len(inpTest)):
                    minn,maxx = SaveImageG(outpTest[ii],'res2/o{}.png'.format(ii))
                    SaveImageG(eval[ii],'res2/r{}.png'.format(ii),minn,maxx)
                    SaveImage(inpTest[ii],'res2/i{}.png'.format(ii))

                if i>0:
                    saver.save(sess, 'model/model')


if __name__ == '__main__':
    tf.app.run()
import tensorflow as tf

def coarse_arch(
    images,
    keep_prob = 0.5,
    is_training = False
    ):
    # layer 1
    coarse1Conv = tf.contrib.layers.conv2d(inputs=images, num_outputs=96, kernel_size=11, stride=4, padding='VALID')
    print(coarse1Conv.get_shape())
    coarse1MP = tf.contrib.layers.max_pool2d(coarse1Conv, kernel_size=[2,2], stride=[2,2], padding='VALID')
    print(1,coarse1MP.get_shape())
    #layer2
    coarse2Conv = tf.contrib.layers.conv2d(inputs=coarse1MP, num_outputs=256, kernel_size=5, stride=1, padding='SAME')
    print(coarse2Conv.get_shape())
    coarse2MP = tf.contrib.layers.max_pool2d(coarse2Conv, kernel_size=[2,2], stride=[2,2], padding='VALID')
    print(2,coarse2MP.get_shape())
    #layer3-5
    coarse3Conv = tf.contrib.layers.conv2d(inputs=coarse2MP, num_outputs=384, kernel_size=3, stride=1, padding='SAME')
    print(3,coarse3Conv.get_shape())
    coarse4Conv = tf.contrib.layers.conv2d(inputs=coarse3Conv, num_outputs=384, kernel_size=3, stride=1, padding='SAME')
    print(4,coarse4Conv.get_shape())
    coarse5Conv = tf.contrib.layers.conv2d(inputs=coarse4Conv, num_outputs=256, kernel_size=3, stride=1, padding='VALID')
    print(5,coarse5Conv.get_shape())
    coarse5MP = tf.contrib.layers.max_pool2d(coarse5Conv, kernel_size=2, stride=2, padding='SAME')
    print(5,coarse5MP.get_shape())
    #layer 6 fc with dropout
    coarse6fc = tf.layers.conv2d(coarse5MP,filters=4096,kernel_size=[6,8], strides=[1,1], padding='VALID') #conv2d instead of fullyconnected
    print(6,coarse6fc.get_shape())
    coarse6drop = tf.contrib.layers.dropout(coarse6fc, keep_prob=keep_prob, is_training = is_training) # default dropout prob 0.5
    print(coarse6drop.get_shape())
    #layer 7 fc
    coarse7fc = tf.contrib.layers.fully_connected(inputs=coarse6drop, num_outputs=4070, activation_fn=None) #activation fn None = linear
    coarse7Out = tf.reshape(coarse7fc, [-1,55,74,1])
    print(coarse7Out.get_shape())
    return coarse7Out

def fine_arch(
    images,
    coarseOut,
    is_training = False
    ):
    #layer 1
    fine1Conv = tf.contrib.layers.conv2d(inputs=images, num_outputs=63, kernel_size=9, stride=2, padding='VALID')
    print(11,fine1Conv.get_shape())
    fine1MP = tf.contrib.layers.max_pool2d(inputs=fine1Conv, kernel_size=2, stride=2, padding='VALID')
    print(12,fine1MP.get_shape())
    # concat coarse 7 to fine 2
    fine2Concat = tf.concat([fine1MP, coarseOut],3)
    print(12,fine2Concat.get_shape())
    #layer 3
    fine3Conv = tf.contrib.layers.conv2d(fine2Concat, num_outputs=64, kernel_size=5, stride=1, padding='SAME')
    print(13,fine3Conv.get_shape())
    #layer 4
    fine4Conv = tf.contrib.layers.conv2d(fine3Conv, num_outputs=1, kernel_size=5, stride=1, padding='SAME', activation_fn=None)
    print(14,fine4Conv.get_shape())
    return fine4Conv  

def lossFunction(logits, depths) :
    #simlified loss function
    return tf.losses.mean_squared_error(logits, depths)

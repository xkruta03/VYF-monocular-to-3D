from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import re
import sys
import tarfile
import random
from PIL import Image
import data_loader as dl
import model as m
from skimage import data, color
from skimage.transform import resize
import constanst as c
import argsloader as al
import numpy as np

import tensorflow as tf

FLAGS = None

def SaveImage(img, name):

    maxx = np.max(img)
    minn = np.min(img)

    img = (img-minn) * (255./(maxx-minn))
    img=np.squeeze(img,2)
    im = Image.fromarray(img).convert('RGB')
    im.save(name)


def main(argv):

    inputFile, outputFile = al.loadArgs(argv)

    gpu_options = tf.GPUOptions(allow_growth=True)

    inputs = tf.placeholder(tf.float32, shape=[None, c.IMAGE_WIDTH, c.IMAGE_HEIGHT,3], name = 'inputs')

    keep_prob = tf.placeholder(tf.float32, name='keep_prob')
    is_training = tf.placeholder(tf.bool, name='is_training')
    

    with tf.variable_scope('coarse_arch'):    
        net = m.coarse_arch(inputs,keep_prob)

    with tf.variable_scope('fine_arch'):    
        logits = m.fine_arch(inputs,net)


    with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:
        sess.run(tf.global_variables_initializer())

        img = data.imread(inputFile)
        img = np.asarray(resize(img, (c.IMAGE_WIDTH,c.IMAGE_HEIGHT)))

        saver = tf.train.Saver()
        print("Restore net")
        saver.restore(sess, save_path="model/model")
        eval = sess.run(logits,feed_dict={
                        inputs: [img],
                        keep_prob: 1, 
                        is_training: False
                        });
        SaveImage(eval[0], outputFile)


if __name__ == '__main__':
    tf.app.run()
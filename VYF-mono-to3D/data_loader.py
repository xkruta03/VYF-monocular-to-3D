import os
import re
import sys
import numpy as np
from PIL import Image
import random

import constanst as c

def load_data(data_directory):
    directories = [d for d in os.listdir(data_directory) 
                   if os.path.isdir(os.path.join(data_directory, d))]
    depths = []
    images = []
    counter = 0
    for d in directories:
        label_directory = os.path.join(data_directory, d)
        file_names_src = [os.path.join(label_directory, f) 
                      for f in os.listdir(label_directory) 
                      if f.endswith("colors.png")]
        file_names_dst = [os.path.join(label_directory, f) 
                      for f in os.listdir(label_directory) 
                      if f.endswith("depth.png")]

        for ii in range(len(file_names_src)):
            img = Image.open(file_names_src[ii])

            images.append(img)
           
            img = Image.open(file_names_dst[ii])
            depths.append(img)

    return images, depths


def DataAugmentation(img, imgDepth):   
    angle = random.randint(-5,5)

    imgR = img.rotate(angle)
    imgRDepth = imgDepth.rotate(angle)
   
    if random.random()>0.5:
        imgR = imgR.transpose(Image.FLIP_TOP_BOTTOM)
        imgRDepth = imgRDepth.transpose(Image.FLIP_TOP_BOTTOM)

    if random.random()>0.5:
        imgR = imgR.transpose(Image.ROTATE_180)
        imgRDepth = imgRDepth.transpose(Image.ROTATE_180)

    imgR = imgR.resize([c.IMAGE_HEIGHT,c.IMAGE_WIDTH],Image.BICUBIC)
    imgRDepth = imgRDepth.resize([c.IMAGE_DHEIGHT,c.IMAGE_DWIDTH],Image.BICUBIC)

    colorCor = (random.random()*0.4) + 0.8 # 0.8-1.2

    imgR = np.asarray(imgR) / 255. * colorCor
    imgRDepth = np.asarray(imgRDepth) / 10000. #magic to get range <1
    imgRDepth = np.expand_dims(imgRDepth,2)
    return imgR, imgRDepth

